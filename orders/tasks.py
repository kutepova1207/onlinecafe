from celery import task
from django.core.mail import send_mail
from .models import Order


@task
def OrderCreated(order_id):
    order = Order.objects.get(id=order_id)
    subject = 'BonAppetit!'
    message = 'Уважаемый(ая) {}!\n Ваш заказ уже готовится!\n Мы свяжемся с Вами с ближайшее время для уточнения времени доставки и других деталей.\n Спасибо, что выбрали Bon Appetit!'.format(order.first_name)
    mail_send = send_mail(subject, message, 'bonnappettite@gmail.com', [order.email])
    return mail_send