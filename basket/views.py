from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_POST
from cafe.models import Dish
from .basket import Basket
from .forms import BasketAddDishForm
from promocods.forms import PromocodeApllyForm


@require_POST
def BasketAdd(request, dish_id):
    basket = Basket(request)
    dish = get_object_or_404(Dish, id=dish_id)
    form = BasketAddDishForm(request.POST)
    if form.is_valid():
        cd = form.cleaned_data
        basket.add(dish=dish, quantity=cd['quantity'],
                                  update_quantity=cd['update'])
    return redirect('basket:BasketDetail')


def BasketRemove(request, dish_id):
    basket = Basket(request)
    dish = get_object_or_404(Dish, id=dish_id)
    basket.remove(dish)
    return redirect('basket:BasketDetail')



def BasketDetail(request):
    basket = Basket(request)
    for item in basket:
    	item['update_quantity_form'] = BasketAddDishForm(
    		       initial={'quantity': item['quantity'],
    		                'update': True})
    promocode_apply_form = PromocodeApllyForm()
    return render(request, 'basket/detail.html', {'basket': basket, 'promocode_apply_form': promocode_apply_form})