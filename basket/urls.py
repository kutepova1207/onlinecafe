from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.BasketDetail, name='BasketDetail'),
    url(r'^remove/(?P<dish_id>\d+)/$', views.BasketRemove, name='BasketRemove'),
    url(r'^add/(?P<dish_id>\d+)/$', views.BasketAdd, name='BasketAdd'),
]