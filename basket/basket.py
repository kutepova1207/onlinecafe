from decimal import Decimal
from django.conf import settings
from cafe.models import Dish
from promocods.models import Promocode




class Basket(object):
    def __init__(self, request):
        self.session = request.session
        self.promocode_id = self.session.get('promocode_id')
        basket = self.session.get(settings.BASKET_SESSION_ID)
        if not basket:
            basket = self.session[settings.BASKET_SESSION_ID] = {}
        self.basket = basket


    @property
    def promocode(self):
        if self.promocode_id:
            return Promocode.objects.get(id=self.promocode_id)
        return None

    def get_discount(self):
        if self.promocode:
            return (self.promocode.discount / Decimal('100')) * self.get_total_price()
        return Decimal('0')

    def get_total_price_after_discount(self):
        return self.get_total_price() - self.get_discount()



    def add(self, dish, quantity=1, update_quantity=False):
        dish_id = str(dish.id)
        if dish_id not in self.basket:
            self.basket[dish_id] = {'quantity': 0,
                                     'price': str(dish.price)}
        if update_quantity:
            self.basket[dish_id]['quantity'] = quantity
        else:
            self.basket[dish_id]['quantity'] += quantity
        self.save()


    def save(self):
        self.session[settings.BASKET_SESSION_ID] = self.basket
        self.session.modified = True


    def remove(self, dish):
      dish_id = str(dish.id)
      if dish_id in self.basket:
          del self.basket[dish_id]
          self.save()


    def __iter__(self):
        dish_ids = self.basket.keys()
        dishs = Dish.objects.filter(id__in=dish_ids)
        for dish in dishs:
            self.basket[str(dish.id)]['dish'] = dish

        for item in self.basket.values():
            item['price'] = Decimal(item['price'])
            item['total_price'] = item['price'] * item['quantity']
            yield item


    def __len__(self):
        return sum(item['quantity'] for item in self.basket.values())


    def get_total_price(self):
        return sum(Decimal(item['price']) * item['quantity'] for item in self.basket.values())

    def clear(self): #очистка сессии
        del self.session[settings.BASKET_SESSION_ID]
        self.session.modified = True