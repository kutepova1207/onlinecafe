from django.apps import AppConfig


class PayConfig(AppConfig):
    name = 'pay'
    verbose_name = 'Оплата'

    def ready(self):
        import pay.signals