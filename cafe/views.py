from django.shortcuts import render, get_object_or_404, render_to_response
from .models import *
from basket.forms import BasketAddDishForm


def DishList(request, category_slug=None):
    category = None
    categories = Category.objects.all()
    dishs = Dish.objects.filter(available=True)
    if category_slug:
        category = get_object_or_404(Category, slug=category_slug)
        dishs = dishs.filter(category=category)
    return render(request, 'cafe/dish/list.html', {
        'category': category,
        'categories': categories,
        'dishs': dishs})


def DishDetail(request, id, slug):
    dish = get_object_or_404(Dish, id=id, slug=slug, available=True)
    basket_dish_form = BasketAddDishForm()
    return render(request,'cafe/dish/detail.html',
                    {'dish': dish,
                    'basket_dish_form': basket_dish_form})


