from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.DishList, name='DishList'),
    url(r'^(?P<category_slug>[-\w]+)/$', views.DishList, name='DishListByCategory'),
    url(r'^(?P<id>\d+)/(?P<slug>[-\w]+)/$', views.DishDetail, name='DishDetail'),
]