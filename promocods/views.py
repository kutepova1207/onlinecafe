from django.shortcuts import render, redirect
from django.utils import timezone
from django.views.decorators.http import require_POST
from .models import Promocode
from .forms import PromocodeApllyForm


@require_POST
def PromocodeApply(request):
    now = timezone.now()
    form = PromocodeApllyForm(request.POST)
    if form.is_valid():
        code = form.cleaned_data['code']
        try:
            promocode = Promocode.objects.get(code__iexact=code,
                                      valid_from__lte=now,
                                      valid_to__gte=now,
                                      active=True)
            request.session['promocode_id'] = promocode.id
        except Promocode.DoesNotExist:
            request.session['promocode_id'] = None
    return redirect('basket:BasketDetail')